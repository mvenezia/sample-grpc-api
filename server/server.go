package main

import (
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"fmt"

	"flag"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"strings"

	api "gitlab.com/mvenezia/sample-grpc-api/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

// server is used to implement helloworld.GreeterServer.
type server struct{}

// GetPoseidon implements helloworld.GreeterServer
func (s *server) SquareNumber(ctx context.Context, in *api.SquareNumberInput) (*api.SquareNumberReply, error) {
	return &api.SquareNumberReply{Result: in.Number * in.Number, Echo: in.Echo}, nil
}

func startServer(addr string, gracefulStop chan os.Signal) error {
	var err error
	log.Print("starting server")

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	var opts []grpc.ServerOption

	grpcServer := grpc.NewServer(opts...)
	api.RegisterJohnBotServer(grpcServer, &server{})

	// Chance here to gracefully handle being stopped.
	go func() {
		sig := <-gracefulStop
		log.Printf("caught sig: %+v", sig)
		log.Println("waiting for 2 second to finish processing")
		time.Sleep(2 * time.Second)
		grpcServer.Stop()
		log.Print("server terminated")
		os.Exit(0)
	}()

	log.Printf("attempting to start server in address: %s", addr)

	return grpcServer.Serve(listener)
}

func main() {
	ctx := context.Background()
	_, cancel := context.WithCancel(ctx)
	defer cancel()

	viper.SetEnvPrefix("john")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// using standard library "flag" package
	flag.Int("port", 5656, "Port to listen on")
	flag.String("host", "0.0.0.0", "IP to list on")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	viper.AutomaticEnv()

	portNumber := viper.GetInt("port") // retrieve value from viper
	host := viper.GetString("host")    // retrieve value from viper

	//  Get notified that server is being asked to stop
	// Handle SIGINT and SIGTERM.
	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGINT, syscall.SIGTERM)

	// Server Code
	err := startServer(fmt.Sprintf("%s:%d", host, portNumber), gracefulStop)
	if err != nil {
		log.Fatalf("failed to start server: %s", err)
	}
}
