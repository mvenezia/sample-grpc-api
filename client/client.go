package main

import (
	"fmt"
	"log"

	pb "gitlab.com/mvenezia/sample-grpc-api/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	log.Println("Client starting")

	serverAddr := fmt.Sprintf("%s:%d", "127.0.0.1", 5656)

	// Set up a connection to the server.
	conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewJohnBotClient(conn)

	r, err := c.SquareNumber(context.Background(), &pb.SquareNumberInput{Number: 2, Echo: "foo", Joe: &pb.JohnValue{Pivot: 5, Uber: "John"}})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Answer: %d, Echo: %s", r.Result, r.Echo)

}
